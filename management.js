const { saveData, loadData } = require('./db');
const { RequestDataSaver } = require('./request_counter');

class ManagementController {
    addTransaction(request, response) {
        const { from, to, amount } = request.body;
        
        RequestDataSaver.addData({ from, to, amount });
        response.sendStatus(200)
    }

    addLink(request, response) {
        const { id, url } = request.body;
        const DB = loadData();

        DB.links.push({id, url});
        saveData(DB);
        response.send('Save!\n');
    }

    status(request, response) {
        const data = loadData();
        const obj = {
            id: 92,
            name: 'SNL_ANADEA',
            last_hash: isBlockContains(data),
            neighbours: data.links.map(link => link.id),
            url: 'http://192.168.44.92:5050'
        }
        
        response.json(obj);
    }

    sync(request, response) {
        const { blocks } = loadData();
        response.json(blocks);
    }
}

function isBlockContains({ blocks }) {
 return  blocks.length ? blocks[blocks.length - 1].block_hash : 0
}

module.exports.ManagementController = new ManagementController()