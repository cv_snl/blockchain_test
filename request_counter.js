const { BlockchainController } = require('./blockchain');

class RequestDataSaver {
  constructor() {
    this.counter = 0
    this.requestBodyArray = []
  }

  addData(data) {
    if (this.counter === 4) {
      this.requestBodyArray.push(data)

      BlockchainController.saveBlock(this.requestBodyArray)
      this.clearData()
    } else {
      this.counter += 1
      this.requestBodyArray.push(data)
    }
  }

  clearData() {
    this.counter = 0
    this.requestBodyArray = []
  }
}

module.exports.RequestDataSaver = new RequestDataSaver()