const fs = require('fs')

const loadData = () => {
   const data = fs.readFileSync('./blocks.json', 'utf-8')
   
   return JSON.parse(data)
}

const saveData = data => {
    fs.writeFileSync('./blocks.json',JSON.stringify(data))
}

module.exports = {
    saveData,
    loadData
}