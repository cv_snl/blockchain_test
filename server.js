const express = require('express');
const bodyParser = require('body-parser');
const rp = require('request-promise');
const { saveData, loadData } = require('./db');
const DB = loadData();

const { ManagementController } = require('./management');
const { BlockchainController } = require('./blockchain');

const App = express();

App.use(bodyParser.json());
App.use(bodyParser.urlencoded({ extended: true }));

App.get('/management/sync', ManagementController.sync);
App.get('/management/status', ManagementController.status);
App.get('/blockchain/get_blocks/:num_blocks', BlockchainController.getBlocks);

App.post('/management/add_transaction', ManagementController.addTransaction);
App.post('/management/add_link', ManagementController.addLink);
App.post('/blockchain/receive_update', BlockchainController.receiveUpdate);

App.listen(5050, () => console.log('Server start on 5050 port'));

const promises = DB.links.map(element => rp.get(element.url + '/management/sync'));

Promise.all(promises).then(array => {
    if (JSON.parse(array).length > DB.blocks.length) {
        DB.blocks = JSON.parse(array);
    }

    saveData(DB);
}).catch(err => console.log('DOWN'));