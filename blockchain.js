const req = require('request');
const crypto = require('crypto');
const { saveData, loadData } = require('./db');

class BlockchainController {
    getBlocks(request, response) {
        const { number } = request.params;
        const { blocks } = loadData();

        if (blocks.length <= number) return blocks.reverse();
    
        response.json(blocks.slice(-number).reverse());
    }

    receiveUpdate(request, response) {
        const { sender_id, block } = request.body
        const data = loadData();

        isBlockExist(data.blocks, block)

        saveData(data);
        response.json({ status: 200 });
    }

    saveBlock(requestValueArray) {
        const data = loadData()
    
        data.blocks.push(this.createBlock(data.blocks, requestValueArray));
        saveData(data);

        data.links.forEach(element => {
            const options = {
                uri: element.url + '/blockchain/receive_update',
                method: 'POST',
                json: { sender_id: 92, block: data.blocks[data.blocks.length - 1] }
            }

            req.post(options);
        });
    }

    createHash(string) {
        const hash = crypto.createHash('sha256')
    
        return hash.update(string).digest('hex')
    }

    createBlock(blocks, rawData) {
        const block = {
            prev_hash:  blocks.length ? blocks[blocks.length - 1].hash : 0,
            tx: rawData,
            ts: Math.floor(new Date() / 1000)
        }
        
        block.hash = this.createHash(`${block.prev_hash}${this.joinTransactionData(block.tx)}${block.ts}`)
    
        return block
    }

    joinTransactionData(array) {
        return array.reduce((acc, curr) => acc += `${curr.from}${curr.to}${curr.amount}`, '')
    }
}

function isBlockExist(blocks, block) {
    const isExist = Array.isArray(blocks.findIndex(item => item.hash === block.hash));

    if (!isExist) {
        if (blocks[blocks.length - 1].hash === block.prev_hash) {
            blocks.push(block);
        } else {
        const parentIndex = blocks.findIndex(item => item.hash === block.prev_hash);
        resolve(parentIndex, blocks, block)
        }

    }

}

function resolve(parentIndex, blocks, block) {
    if (parentIndex !== -1) {
        let tail = blocks.splice(parentIndex + 1);

        blocks.push(block);
        blocks = blocks.concat(tail);
    }

}

module.exports.BlockchainController = new BlockchainController();